In 'Matlab code':
	- thetatox.m:		 	A Matlab function that is used in Simulink to convert a setpoint in theta to a matching
					setpoint in x.
	- seesaw_2012_real.slx:  	The template given for this assignment in which we put our practical LQR controller. This was used for the
					experiments on the practical seesaw.
	- ssmodel.m:			Creates the linear continuous-time state space model of the seesaw.
	- seesaw_practical.slx: 	The Simulink file that contains the practical seesaw model including quantization, AD, computing derivatives, ..
	- seesaw_lqr.slx:		The Simulink file that contains the first, basic LQR controller without quantization, ....
	- plot_data.m:			Plots the states x, theta, xdot, thetadot given experimental data. This was used to generate a few plots in the report
					based on data gathered from the seesaw.
	- openloopanalysis.m:   	Small script that checks stability, controllability, .. of the continuous-time seesaw model.
	- noisevariance.m:		Small script used to compute the noise variance of the zero input output of the seesaw, as discussed in the report.
	- lqrcontroller_practical.m	Script used to fine-tune the LQR controller on the realistic seesaw model. It also checks stability of the closedloop system.
	- lqrcontroller.m		Small script used to fine-tune the LQR controller on the basic seesaw model.
	- disturbance_rejection_analysis.m: Makes a plot of the data gathered during the disturbance rejection tests.
	- cutoff_freq_analysis.m	Plots the data gathered while doing cut-off frequency tests on the mechanical setup.
    	- real_data.mat     All the data gathered during the sessions and used in the report.