t = noise_meaurement.time;
x = noise_meaurement.signals(1).values;
theta = noise_meaurement.signals(2).values;

plot(t, x); hold on;
plot(t, theta); hold on;
xlabel('Time(s)');
ylabel('System output');
h = legend('$x$', '$\theta$');
set(h, 'Interpreter', 'Latex');