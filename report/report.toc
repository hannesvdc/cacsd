\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Model and open loop analysis}{1}{section.2}
\contentsline {section}{\numberline {3}LQR Controller based on exact state feedback}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}LQR model in \texttt {Simulink}}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Choosing the weighing matrices $\boldsymbol {Q}$ and $\boldsymbol {R}$}{3}{subsection.3.2}
\contentsline {section}{\numberline {4}LQR Controller based on measured state feedback}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}LQR model in \texttt {Simulink}}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Choosing the weighting matrices $\boldsymbol {Q}$ and $\boldsymbol {R}$}{7}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Effect of the cut-off frequency on the controller}{9}{subsection.4.3}
\contentsline {section}{\numberline {5}Experimental results ("The proof of the pudding")}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}LQR model in \texttt {Simulink}}{11}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Design of the LQR controller}{13}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Tuning the practical LQR controller}{13}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Explanations for the deviations between simulations and experiments}{16}{subsubsection.5.2.2}
\contentsline {subsection}{\numberline {5.3}Determining the noise variance}{17}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Disturbance rejection}{18}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Influence of the cut-off frequency}{18}{subsection.5.5}
\contentsline {section}{\numberline {6}Conclusion}{18}{section.6}
