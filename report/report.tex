\documentclass[a4paper,kul]{kulakarticle}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{float}
\usepackage{epstopdf}
\usepackage{rotating}
%\usepackage{gensymb}
\usepackage{booktabs}

\date{Academic year 2016-2017}
\address{
	Master Mathematical Engineering \\
	CACSD \\
	Prof. Dr. Ir. De Moor, Dr. Ir. Agudelo}
\title{Report Practical Session: Seesaw}
\author{Simon Geirnaert, Hannes Vandecasteele}

\begin{document}
	
\maketitle

\tableofcontents

\section{Introduction}
In this report we will design a controller for a mechanical seesaw. The goal of the project is to design the controller in such a way that the seesaw can follow a given setpoint and is also robust against small kicks against the seesaw. In the report, we first assume that the exact states are known for the LQR controller, after which we will numerically calculate certain states. The largest part of the report is devoted to testing the controller on the mechanical setup and the differences between simulation and practice.

\section{Model and open loop analysis}
\label{sec:model}
By filling in the numerical values of the parameters in the model derived in the assignment, we find the following the state-space model of the seesaw-system:

\begin{align*}
\begin{bmatrix}
\dot{x} \\
\dot{\theta} \\
\ddot{x}\\
\ddot{\theta}
\end{bmatrix} & = 
\begin{bmatrix}
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 \\
-1.6143 & -9.1618 & -16.9249 & 0 \\
-12.9144 & 5.1856 & -2.7290 & 0
\end{bmatrix}
\begin{bmatrix}
x \\
\theta \\
\dot{x} \\
\dot{\theta} 
\end{bmatrix} + 
\begin{bmatrix}
0 \\
0 \\
3.3827\\
0.5454
\end{bmatrix} V, \\
\begin{bmatrix}
V_x \\
V_{\theta} \\
\end{bmatrix} & = 
\begin{bmatrix}
9.6711 & 0 & 0 & 0 \\
0 & 12.0932 & 0 & 0
\end{bmatrix} \begin{bmatrix}
x \\
\theta \\
\dot{x} \\
\dot{\theta} 
\end{bmatrix} + 
\begin{bmatrix}
0 \\
0
\end{bmatrix} V.
\end{align*}

The state variables of the model are the position of the cart $x$ (in $\si{\meter}$), the angle of the seesaw $\theta$ (in $\si{\radian}$) and the corresponding derivatives $\dot{x}$ and $\dot{\theta}$. The variable $V$ is the input voltage of the DC motors. The outputs of the system are the voltages that are measured by the sensors. That's why the matrix $C$ has the conversion factors from $x$ to $V_x$ and $\theta$ to $V_{\theta}$ on the diagonal.

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{images/pzmap}	
	\caption{The pole-zero map of the open-loop system}
	\label{fig:pzmap}
\end{figure}

\vspace{2mm}
\noindent
Figure \ref{fig:pzmap} shows the pole-zero map of the open-loop system. There are no transmission zeros and only three of the four poles (4\textsuperscript{th}-order system) ($2.9006, -1.5310 + 0.5114i, -1.5310 - 0.5114i, -16.7635$) have a negative real part. Hence, the system is unstable. The observability matrix $\boldsymbol{O} \in \mathbb{R}^{8 \times 4}$ has rank 4, so the system is observable. The controllability matrix $\boldsymbol{C} \in \mathbb{R}^{4 \times 4}$ is of rank 4, so the system is also controllable. These are necessary conditions to start control. We know now that the system is stabilizable (all unstable modes are controllable) and detectable (all unstable modse are observable). We also have a minimal system, because the state-space model is controllable and observable.

\vspace{2mm}
\noindent
The main goal of controlling this system is \emph{stabilization}. The equilibrium positions of the seesaw are inherently unstable and by controlling the cart, we want to damp small \emph{disturbances} around the equilibrium. Another control goal is tracking a sequence of setpoints, more than tracking a time-varying reference signal. Also important is \emph{precision}: the error between the obtained position and the reference setpoint should be at least as good as the sensor precision. \emph{Fast response} is minor importance relative to the other control goals.

\section{LQR Controller based on exact state feedback}
In this part of the assignment we assume that the state vector $\mathbf{x} = \begin{bmatrix} x & \theta & \dot{x} & \dot{\theta}\end{bmatrix}^T$ is known and can be used for feedback. The output matrix $\boldsymbol{C}$ is hence
\[
\boldsymbol{C} = \begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 
\end{bmatrix}.
\]

Note that the outputs are now in SI units and not in voltage anymore.

\subsection{LQR model in \texttt{Simulink}}
The goal is to find a feedback matrix $\boldsymbol{K}$ based on an LQR strategy such that the system will atain a certain angle, given as a reference input. The complete \texttt{Simulink} diagram is given in Figure \ref{fig:simulink_lqr}.

\begin{figure}
	\includegraphics[width=\linewidth]{images/simulink_lqr}
	\caption{Simulink model for the continuous-time LQR controller of the seesaw.}
	\label{fig:simulink_lqr}
\end{figure}

In the figure, the following blocks were used:
\begin{itemize}
	\item The continuous-time model with given matrices $\boldsymbol{A}$, $\boldsymbol{B}$, $\boldsymbol{C}$ and $\boldsymbol{D}$. Note here that $\boldsymbol{C} = \boldsymbol{I}$ such that the outputs are the states and can be used for feedback. The matrices $\boldsymbol{A}$ and $B\boldsymbol{argument}$ were computed in the previous section.
	\item The gain matrix $\boldsymbol{K}$ computed using the LQR method. This will further be described in the next section. It is important to note that $\boldsymbol{K}$ controls not the real output of the system, but the difference of the states and the reference signal. This is logical since we want the system to track a given angle $\theta$, hence we are penalizing the difference.
	\item The gain matrix $\boldsymbol{N}_x$ is given by $
\boldsymbol{N}_x = \begin{bmatrix}
1&0&0&0\\
0&1&0&0\\
0&0&0&0\\
0&0&0&0
\end{bmatrix}
$ and is used to transform the two reference signals $\theta_d$ and $x_d$ to four states, by demanding that $\dot{x}_d=0$ and $\dot{\theta}=0$. This is necessary because we want the system to move in steady-state.

	\item Finally, we defined a MATLAB function that transforms a given steady-state angle $\theta$ to $x$ because these parameters are not independent of each other and have to be matched. The code for this function can be found in \texttt{thetatox.m}. Assuming steady-state, we get the following formula for $x_{\text{desired}}$ in function of $\theta_{\text{desired}}$:
	\[
	x_{\text{desired}} = \frac{M_cD_t+M_{sw}D_c}{M_c}\tan(\theta_{\text{desired}}).
	\]
\end{itemize}

\subsection{Choosing the weighing matrices $\boldsymbol{Q}$ and $\boldsymbol{R}$}
To implement an LQR controller, we need penalizing matrices $\boldsymbol{Q}$ for the states and $\boldsymbol{R}$ for the inputs. We only consider diagonal elements because this is the most simple case. Off-diagonal elements indicate a dependence between two states and this can be hard to find. We will start from the matrices given in the assignment and then follow a trial-and-error procedure to converge to good matrices.

Suppose we have 
\[
\boldsymbol{Q} = \begin{bmatrix}
1000&0&0&0\\
0&4000&0&0\\
0&0&0&0\\
0&0&0&0\\
\end{bmatrix},
\boldsymbol{R} = 10.
\]
If we apply a step input of $1^\circ$ for $\theta$, we get the simulation results shown in Figure \ref{fig:r10}. 	


\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{images/Q1000400000R10}
	\caption{States for the first experiment}
	\label{fig:r10}
\end{figure}
We can gain a lot of physical insight into the system from these simulations. First of all, when we increase the setpoint for $\theta$, the cart seems to slide down instead of going up. This is logical since the cart first needs to go in the opposite direction to tild the seesaw and then it has to go to the other side to balance the center of mass of the total system such that the system is in equilibrium. Hence the speed $\dot{x}$ is also negative in the beginning. After half a second, the controller starts controlling $x$ as well and it increases to the final value. The speed becomes positive first before it settles at zero. This is exactly what we may expect intuitively.

Note that in the simulation there is a small steady-state error for $x$ and $\theta$. This can be solved by reducing $\boldsymbol{R}$ to 1 such that the state deviations have a higher weight in the LQR controller. The results are shown in Figure \ref{fig:experiment2}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{images/Q1000400000R1}
	\caption{States for the second experiment, the steady-state error is reduced by a fair amount.}
	\label{fig:experiment2}
\end{figure}
We see indeed that the steady-state error on $x$ and $\theta$ is reduced significantly. The remaining steady-state error is good enough to continue working with.
There is however still a large overshoot on $x$ and $\theta$. We can tackle this by increasing the velocity penalties in $\boldsymbol{Q}$ to 300 for example. This gives the following resulting weighting matrices:
\[
\boldsymbol{Q} = \begin{bmatrix}
1000&0&0&0\\
0&4000&0&0\\
0&0&300&0\\
0&0&0&300\\
\end{bmatrix},
\boldsymbol{R} = 1.
\]
The simulation results are shown in Figure \ref{fig:Q10004000100100R1}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{images/Q10004000300300R1}
	\caption{States for the third experiment, the overshoots for $x$ and $\theta$ are negligible.}
	\label{fig:Q10004000100100R1}
\end{figure}
The settling time for the system is about 2 seconds, wich is good enough in practice. Another important thing to consider is that the input voltages are not higher than the saturation voltage of 3.5V. The input of the final system is shown in Figure \ref{fig:inputslqr}.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/systeminput_lqr}
	\caption{The inputs of the seesaw for the final controller from section 2.2. The inputs remain quite low.}
	\label{fig:inputslqr}
\end{figure}
From the figure, we learn that the inputs are very low, they only peak to 1.2V, which is far below the \SI{5}{\volt} saturation mark. Figure \ref{fig:poles_lqr} confirms that the obtained closed-loop system is stable. 

We can conclude that the designed controller is stable, has a good precision due to the very small steady-state error and is relatively fast. This is a good starting point for the more realistic model.

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{images/poles_lqr}
	\caption{The poles of the closed-loop system designed with an LQR controller, with the full state vector available}
	\label{fig:poles_lqr}
\end{figure}

\section{LQR Controller based on measured state feedback}
We now assume that the outputs are measured in the real setup and that we have to derive the full state vector.

\subsection{LQR model in \texttt{Simulink}}
Figure \ref{fig:simulink_practical_lqr} shows the complete diagram of the seesaw model with LQR control when we assume that the outputs of the system are the measured outputs $V_x$ and $V_{\theta}$ from the real setup.

\begin{sidewaysfigure}
	\includegraphics[width=1\linewidth]{images/simulink_practical_lqr}
	\caption{\texttt{Simulink} model for the continuous-time LQR controller of the seesaw, based on the outputs of the real system}
	\label{fig:simulink_practical_lqr}
\end{sidewaysfigure} 
It contains the following elements:
\begin{itemize}
	\item The continuous-time model, given by matrices $\boldsymbol{A}, \boldsymbol{B}, \boldsymbol{}$ and $\boldsymbol{D}$. Now $\boldsymbol{C}$ and $\boldsymbol{D}$ are the same output and direct transmission matrices as in Section \ref{sec:model}. This is because it the real model, we also only measure $x$ and $\theta$ in volts.
	
	\item At the output of the continuous time model, white noise is added. We take the magnitude of the measurement noise at a maximum of $10^{-6}$. We take this maximal value because for white noise, the power spectrum is constant, such that the variance of the noise added to the output is proportional with the noise power (with a factor proportional to the bandwidth). Because the order of the output signals of $x$ and $\theta$ is of $\mathcal{O}(10^{-2})$ and we don't want to drown the signal in the noise, so we want the standard deviation of the signal to be an order lower then the order of the signal itself. This corresponds with a variance of $10^{-6}$.
	
	\item We implement the A/D converter with a Zero-Order Hold (with $T_s = \SI{5}{\milli \second}$), saturate the signal at $\SI{-10}{\volt}$ and $\SI{10}{\volt}$ and quantize it with a resolution of 16 bits. Because the range is $\SI{20}{\volt}$ and we have $2^ {16}$ values, the length of the quantization intervals is equal to $\frac{20}{2^{16}} = \num{3.0518e-04}$. We also convert the units ($\si{\volt}$) to SI units ($\si{\meter} \text{ and } \si{\radian}$).
	
	\item The low-pass filter, with cut-off frequency of $\SI{2}{\hertz}$, is applied to the raw measurements before calculating the difference. First we filter the discrete measurements $y_n$, whereafter we add it to the delayed and filtered $y_{n-1}^f$ values. We then use $y_{n}^f$ and $y_{n-1}^f$ to calculate the states $\dot{x}$ and $\dot{\theta}$, whereafter we assemble all the states in one big state vector.
	
	\item The inputs for the continuous-time system are saturated between -5 and 5 volts, according to the assignment.
\end{itemize}

The other blocks are mostly the same as in the academic model, including:
\begin{itemize}
	\item LQR gain matrix $\boldsymbol{K}$ is the same block as in the previous model, with different weighting matrices, as discussed in the following subsection. The obtained state vector is then used in the same feedback loop as described in the previous section (see also Figure \ref{fig:simulink_lqr}).
	\item The \texttt{MATLAB} function converting a setpoint for $\theta$ to a setpoint for $x$.
\end{itemize}

\subsection{Choosing the weighting matrices $\boldsymbol{Q}$ and $\boldsymbol{R}$}
Due to the fact we no longer have all states available exactly, we may need to retune the LQR controller. We first use the same LQR controller as in the previous section, namely:
\[
\boldsymbol{Q} = \begin{bmatrix}
1000 & 0 & 0 & 0 \\
0 & 4000 & 0 & 0 \\
0 & 0 & 300 & 0 \\
0 & 0 & 0 & 300 \\
\end{bmatrix},
\boldsymbol{R} = 1.
\] The simulations results (with noise power of $10^{-8}$), with the same step change as before, are shown in Figure \ref{fig:Q10004000300300R1practical}. The results are very bad: there is a very large steady-state error for $\theta$ and the states behave very nervously and noisy. It even seems that the system has become unstable.

\begin{figure}
	\centering
	\includegraphics[width = 0.8\textwidth]{images/Q10004000300300R1practical}
	\caption{The simulation results for the more realistic setup, with computation of $\dot{x}$ and $\dot{\theta}$. The weighting matrices are the same as in the previous experiment}
	\label{fig:Q10004000300300R1practical}
\end{figure}

To stabilize the system, we have to adapt the weighting matrices $\boldsymbol{Q}$ and $\boldsymbol{R}$ to obtain better results for the state feedback. Because the steady-state error is that large, we want to put more weight to the states $x$ and $\theta$. We do this by lowering the weights of $\dot{x}$ and $\dot{\theta}$ to 100, such that the relative weight for $x$ and $\theta$ increases. Also by lowering the gains, we drive the system away from instability. We obtain the following weighting matrices:
\[
\boldsymbol{Q} = \begin{bmatrix}
1000 & 0 & 0 & 0 \\
0 & 4000 & 0 & 0 \\
0 & 0 & 100 & 0 \\
0 & 0 & 0 & 100 \\
\end{bmatrix},
\boldsymbol{R} = 1.
\]

The new simulation results, with the same step reference and noise power, are shown in Figure \ref{fig:Q10004000100100R1practical}. The behaviour is now as expected: we have a stable closed-loop system with almost no steady-state error on the objective for $\theta$ of $1^{\circ}$.

We can however show that we can never fully eliminate the steady-state error. To obtain the desired states $\boldsymbol{x}_d = \begin{bmatrix}
x_{\text{desired}} & \theta_{\text{desired}} & 0  & 0
\end{bmatrix}^T$, we calculate the state-feedback $u_k = -\boldsymbol{K}(\boldsymbol{x}_k-\boldsymbol{x}_d)$.  Take the state equation $\boldsymbol{x}_{k+1} = \boldsymbol{A}\boldsymbol{x}_k+\boldsymbol{B}\boldsymbol{u}_k$ and insert the feedback: $\boldsymbol{x}_{k+1} = \boldsymbol{A}\boldsymbol{x}_k-\boldsymbol{B}\boldsymbol{K}(\boldsymbol{x}_k-\boldsymbol{x}_d)$. To obtain the steady-state value $\boldsymbol{x}_{ss}$, we put $\boldsymbol{x}_{k+1} = \boldsymbol{x}_k = \boldsymbol{x}_{ss}$ and obtain: $\boldsymbol{x}_{ss} = [\boldsymbol{I}-(\boldsymbol{A}-\boldsymbol{B}\boldsymbol{K})]^{-1}\boldsymbol{B}\boldsymbol{K}\boldsymbol{x}_d$. Consider $K$ to be a number, than if $K \rightarrow \infty$, the gain matrix for $x_d$ becomes something like $\frac{K}{1+K}$, which does not converge to 1 (or in general to $\boldsymbol{I}$). This is why we cannot avoid the steady-state error theoretically.
\begin{figure}
	\centering
	\includegraphics[width = 0.8\textwidth]{images/Q10004000100100R1practical}
	\caption{The simulation results for the more realistic setup, with computation of $\dot{x}$ and $\dot{\theta}$ and new weighting matrices}
	\label{fig:Q10004000100100R1practical}
\end{figure}


\vspace{2mm}
\noindent
To complete our analysis, we also show the inputs for this final controller in Figure \ref{fig:input_practical}. The inputs are again bounded and never reach the saturation level.
Figure \ref{fig:poles_lqr_practical} shows that the closed-loop system is indeed stable: all poles lie in the left-half plane. We can conclude that the LQR controller on the more realistic model is stable and has a high precision due to the small steady-state error. The reaction time is less than a second which is good in practice. We will use this controller as a first try on the mechanical setup.
\begin{figure}
	\centering
	\includegraphics[width = 0.8\textwidth]{images/input_practical}
	\caption{The inputs of the final controller for more realistic setup}
	\label{fig:input_practical}
\end{figure}


\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{images/poles_lqr_practical}
	\caption{The poles of the closed-loop system designed with an LQR controller, based on the realistic setup}
	\label{fig:poles_lqr_practical}
\end{figure}

\subsection{Effect of the cut-off frequency on the controller}
\label{sec:cutoff}
In all previous experiments, the cut-off frequency in the low-pass filter for the terms in the derivatives was 2Hz. Also in the previous experiments, we kept the noise level to $10^{-8}$. This is of course realistic but to clearly see the impact of the noise when we vary the cut-off frequency, we increase it to $10^{-5}$. For completeness, the results for the matrix
\[
\boldsymbol{Q} = \begin{bmatrix}
1000 & 0 & 0 & 0 \\
0 & 4000 & 0 & 0 \\
0 & 0 & 100 & 0 \\
0 & 0 & 0 & 100 \\
\end{bmatrix},
\boldsymbol{R} = 1.
\]
for a noise level of $10^{-5}$ are shown in Figure \ref{fig:Q10004000100100R1practical2hz}. We see that, compared to Figure \ref{fig:Q10004000100100R1practical}, the oscillations around the steady-state are higher. This is of course logical.\\
If we increase the cut-off frequency by two orders to 200Hz for example, we expect that the system will be prone to more noise and that the amplitude of the oscillations will rise. This is shown in Figure \ref{fig:Q10004000100100R1practical200hz}. The system indeed osillates more but not much more, the effects are mostly visible on $\dot{\theta}$ and $\dot{x}$. Thus for higher frequencies, the system seems to be quite noise resilient.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{images/Q10004000100100R1practical2hz}
\caption{System output for the LQR matrices of section 3.2. The oscillations have a higher amplitude.}
\label{fig:Q10004000100100R1practical2hz}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\linewidth]{images/Q10004000100100R1practical200hz}
\caption{System response for a cut-off frequency of 200Hz. The oscillations are a higher, especially for $\dot{x}$ and $\dot{\theta}$.}
\label{fig:Q10004000100100R1practical200hz}
\end{figure}

Finally we can also check what happens if we decrease the noise cut-off frequency to 0.2Hz. In Figure \ref{fig:Q10004000100100R1practical02hz} we see that the system oscillates very heavily and the amplitude increases. There is no steady-state anymore. We conclude that the system dynamics are also filtered by the low-pass filter used to compute the derivates of $x$ and $\theta$ numerically. We can also conlude that the choise of $\SI{2}{\hertz}$ for $w_c$ is a good trade-off between noise reduction and system dynamics.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{images/Q10004000100100R1practical02hz}
\caption{System response for a cut-off frequency of 0.2Hz. The system dynamics is also filtered by the low-pass filter.}
\label{fig:Q10004000100100R1practical02hz}
\end{figure}

\section{Experimental results ("The proof of the pudding")}
We will now try to implement the control system on the real mechanical setup. 

\subsection{LQR model in \texttt{Simulink}}
Figure \ref{fig:simulink_real_setup} shows the diagram of the LQR model used in the real-time setup.

\begin{sidewaysfigure}
	\includegraphics[width=1\linewidth]{images/simulink_real_lqr}
	\caption{\texttt{Simulink} model for the continuous-time LQR controller of the seesaw, used in the real setup}
	\label{fig:simulink_real_setup}
\end{sidewaysfigure} 

We highlight some elements:
\begin{itemize}
	\item On the outputs, the same filtering as in the previous setup (Figure \ref{fig:simulink_practical_lqr}) is done. Note that we now don't have a zero-order hold, saturation or quantizer, because this is all incorporated in the measurement system of the seesaw.
	\item The reference input ($\theta_{\text{desired}}$, given in degrees) again has to be converted to the corresponding $x_{\text{desired}}$. The problem here is that we didn't manage to use a similar block as in the previous schemes to implement \texttt{thetatox}, because of the \texttt{MATLAB}-version (2012b). But because the correspondence is non-linear, we solved this by replacing this with a simple gain factor (\texttt{T2X}) that implements the following formula: $x_{\text{desired}} = \frac{M_cD_t+M_{sw}D_c}{M_c}\theta_{\text{desired}}$. This is valid because $\theta$ is small and then $\tan \theta \approx \theta$. For example, if $\theta = 2^\circ$ (the maximum setpoint), than the exact $x$ is equal to 0.018387128243924, while the approximation is equal to 0.018379659584463. The approximation is still correct up to six digits and this is beyond the noise level of the seesaw.
	\item Other parts stay the same as in the previous scheme (the gain matrix is again different, see the next section). Only now the outputs of the controller are fed to the \texttt{Analog Output SeeSaw} block.
\end{itemize}

\subsection{Design of the LQR controller}
Figure \ref{fig:originalpractical} shows the output of the real setup using the controller we discussed in Section 3.2. We can remark a few things:
\begin{itemize}
	\item Steady-state error: If we change the setpoint from 1 degree to -1 degree, the system does not seem to reach an equilibrium point, and if it would, it would be far from the real setpoint. This calls for increasing the weight on $\theta$. Also note that this steady-state error is a lot larger than for the simulated system in Figure \ref{fig:Q10004000100100R1practical}
	\item Oscillations: The system seems to oscillate heavily around the applied setpoint, meaning that we have to increase the weights on the derivative states. This oscillation was not present in simulated results in Figure \ref{fig:Q10004000100100R1practical}.
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{images/exp/originalpractical}
\caption{Experimental results for the simulation-based controller on the real setup.}
\label{fig:originalpractical}
\end{figure}

\subsubsection{Tuning the practical LQR controller}
To get rid of the oscillations, we can increase the weight of $\dot{\theta}$ in $\boldsymbol{Q}$ to 1100 for example. The experimental results are shown in Figure \ref{fig:practical1100}.

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{images/exp/practical1100}
\caption{Experimental outputs for the controlled system with 1100 as velocity component in $\boldsymbol{Q}$.}
\label{fig:practical1100}
\end{figure}

Notice that indeed the oscillations died out but the steady-state error is immense. To get rid of the steady-state error, we increase the component in $\boldsymbol{Q}$ corresponding to $\theta$ to 20000. The experimental results are in Figure \ref{fig:practical20000}. 

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{images/exp/practical20000}
\caption{System output when we increase the $\boldsymbol{Q}$ component corresponding to $\theta$ to 20000.}
\label{fig:practical20000}
\end{figure}

An interesting observation is that when we apply a setpoint of $2^{\circ}$, the controller is able to follow the setpoint quite well, but when we change to $-2^{\circ}$, it is not. There is some asymmetry present in the controlled system.

\vspace{2mm}
\noindent
Up to now, we only increased the components in $\boldsymbol{Q}$ corresponding to $\theta$ and $\dot{\theta}$, but in fact $x$ and $\dot{x}$ are proportional to $\theta$ and $\dot{\theta}$. To this end, we used the same conversion factor $\frac{0.456}{15}$ to go from $\theta$ to $x$. The experimental results are shown in Figure \ref{fig:practicalconversion}.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/exp/practicalconversion}
	\caption{Experimental results for a proportionality constant between the $x$ and $\theta$ component in $\boldsymbol{Q}$, as well as for the derivatives.}
	\label{fig:practicalconversion}
\end{figure}
\noindent
Notice that there is again an asymmetry between positive and negative setpoints, but that there remains a very large steady-state error. We may try to overcome this problem by increasing the component of $\theta$ in $Q$ to 30000 (and thus also $x$), but this didn't improve the results.
\vspace{2mm}




We can conclude that we tried different changes to our simulated controller in the practical setting, but that none of the tested controllers are satisfying. Some of them are prone to oscillatory behavior, while others have a large steady-state error. Therefor we chose to continue with the LQR controller where the $\theta$ component is 20000 and for $\dot{\theta}$ 1100.

\vspace{2mm}
\noindent
A better option instead of an LQR controller for the seesaw may be LQR with integral control. In this way, we can get rid of the steady-state error as time goes to infinity and the oscillations can be reduced as well using this approach. We did not have the time to implement and test this integral controller, unfortunately.

\subsubsection{Explanations for the deviations between simulations and experiments}
As stated above, there are a few differences between the simulated results in Figure \ref{fig:Q10004000100100R1practical} and the practical system. The steady-state error in practice seems to be a lot larger than for the simulations and the controller on the real setup gives rise to oscillations as well. One main reason may be that the model is not elaborate enough to describe reality in enough detail. Possible causes are:
\begin{itemize}
	\item \emph{Friction}: In the model for the seesaw, the friction between the cart and the seesaw is not included. This explains why we sometimes hear the motor buzzing while the cart is not moving. As a consequence, the seesaw will reach a different final value and this will have an effect on the steady-state error.
	
	\item \emph{Non-linearities}: The true physical model for the seesaw is non-linear, but for small angles, a linear model seems good enough. The non-linear terms in the equations may start to take over when we increase the angle of the seesaw beyond $2^{\circ}$, also adding to the steady-state error.
	
	\item \emph{Minimum voltage for the actuators}: Sometimes the inputs for the physical system are quite small, such that the motors will not move. The simulated model however assumes that every small input to the system gives rise to a proportional change in $\theta$ or $x$. This does not have to be the case in the mechanical system, also adding to the steady-state error.
	
	\item The wheels of the cart on the seesaw consist of a finite number of gears, meaning that we cannot track every angle $\theta$ accurately. Adding the friction to this also adds to the steady-state error.
	
	\item \emph{Calibration}: Calibrating the angle and position of the cart is of utter importance but very difficult to carry out precisely. This may also have an effect on the steady-state error when the deviations from the setpoint are small.
	
	\item \emph{Sample time}: The position of the seesaw is sampled at $\SI{200}{\hertz}$, or 5ms. The system itself is of course in continuous time, so this causes a modeling error while designing the controller. This will however have only a small effect on the performance of the seesaw since the sampling frequency is quite high.
\end{itemize}

\subsection{Determining the noise variance}
In our simulations we applied a relatively high noise variance of $10^{-6}$ for $x$ as well as $\theta$ and we noticed that the simulated results were quite good with this noise level. We will now estimate the noise level experimentally. To this end, we apply a zero input (zero angle $\theta$ ) and calculate numerically the variance of the output. This will then give us the variance of the output noise. We hence assume there is no process noise. \\
Figure \ref{fig:zeroinput} gives the output of the system when we apply a zero reference input.
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{images/zeroinput}
\caption{The system outputs for a zero input.}
\label{fig:zeroinput}
\end{figure}
We can conclude a few things. First of all, it takes some time before the systems settles in the correct position because the initial condition was not exactly zero. We should not take this transient into account when calculating the noise variance. Realistically we should only sample data after 15 seconds.
There is also a steady-state error present after 15 seconds as in all previous experiments, meaning that we have to calculate the variance of the deviations from this final value. When we do this, we get a variance of $\num{2.5e-6}$ on $\theta$ and $\num{3e-9}$ on $x$. These noise levels are an order smaller than the noise we imposed on the practical controller from Section 4. This controller had decent noise-rejection capacities. We conclude that the noise levels are lower than we expected during simulations, so it is positive that our simulated controller can deal with higher noise levels. The noise however cannot serve as an explanation for the differences between simulations and experiments.
	
\subsection{Disturbance rejection}

An important control goal is robustness against disturbances, that is, a good \emph{disturbance rejection}. To evaluate the disturbance rejection for our controller, we apply two small kicks around $t = \SI{4}{\second}$ and $t = \SI{13.5}{\second}$, in opposite directions.
Figure \ref{fig:disturbance_rej} shows the results. The setpoint is $\theta_{\text{desired}} = 5^\circ$ and corresponding $x_{\text{desired}} = 0.046$. The outputs $\theta$ and $x$ are shown in Figures \ref{fig:dist_theta} and \ref{fig:dist_x}. Again the steady-state error and oscillating outputs are visible. 

\begin{figure}
	\centering
	\begin{subfigure}{1\textwidth}
		\centering
		\includegraphics[width=0.75\textwidth]{images/dist/theta}
		\caption{$\theta$}
		\label{fig:dist_theta}
	\end{subfigure}

	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/dist/x}
		\caption{$x$}
		\label{fig:dist_x}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/dist/in}
		\caption{Inputs}
		\label{fig:dist_in}
	\end{subfigure}

	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/dist/thetadot}
		\caption{$\dot{\theta}$}
		\label{fig:dist_xdot}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/dist/xdot}
		\caption{$\dot{x}$}
		\label{fig:dist_thetadot}
	\end{subfigure}
	\caption{A disturbance rejection test}
	\label{fig:disturbance_rej}
\end{figure}

At the time instances of the kicks, we see large corrections by the inputs (Figure \ref{fig:dist_in}). They even have to be clipped. At $t = \SI{13.5}{\second}$, this causes the cart to go opposite direction (see Figures \ref{fig:dist_x} and \ref{fig:dist_xdot}) and overcompensate the disturbance. It though quickly recovers from the disturbance: for the second, largest kick, after about $\SI{3.5}{\second}$ the disturbance is rejected. We conclude by saying that we are satisfied with the disturbance rejection capacity of our controller.

\subsection{Influence of the cut-off frequency}
In this section, we try to confirm the simulations of Section \ref{sec:cutoff}. First, we lower the cut-off frequency to $\SI{0.2}{\hertz}$, just as in the simulations. The results are shown in Figure \ref{fig:cf_test_low}. Just as in the simulations, we obtain an unstable controller, with growing oscillations. The low-pass filter filtered also system dynamics away.

\begin{figure}
	\centering
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/cutoff/x}
		\caption{$x$}
		\label{fig:cfl_x}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/cutoff/thetadot}
		\caption{$x$}
		\label{fig:cfl_xdot}
	\end{subfigure}

	\begin{subfigure}{0.5\textwidth}
	\centering
	\includegraphics[width=1\textwidth]{images/cutoff/theta}
	\caption{$\theta$}
	\label{fig:cfl_theta}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/cutoff/xdot}
		\caption{inputs}
		\label{fig:cfl_thetadot}
	\end{subfigure}
	\caption{State response for a cut-off frequency of 0.2Hz}
	\label{fig:cf_test_low}
\end{figure}

We also test the case where the cut-off frequency is too large. Figure \ref{fig:cfh} shows some representative plots to show the difference between a high cut-off frequency of $\SI{2000}{\hertz}$ and a normal cut-off frequency of $\SI{2}{\hertz}$. Figures \ref{fig:cfh_xdot} and \ref{fig:cfh_xdot_normal} show that when the cut-off frequency is high, the system is more prone to noise and nervous, as in the simulations (Figure \ref{fig:Q10004000100100R1practical200hz}). This however does not impact much the behaviour of the controller, as is shown in Figure \ref{fig:cfh_theta}: $\theta$ is still a constant (given a constant reference).

\begin{figure}
	\centering
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/cutoff/xdot_high}
		\caption{$\dot{x}$, cut-off frequency $\SI{2000}{\hertz}$}
		\label{fig:cfh_xdot}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/cutoff/xdot_normal}
		\caption{$\dot{x}$, cut-off frequency $\SI{2}{\hertz}$}
		\label{fig:cfh_xdot_normal}
	\end{subfigure}

	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=1\textwidth]{images/cutoff/theta_high}
		\caption{$\theta$, cut-off frequency $\SI{2000}{\hertz}$}
		\label{fig:cfh_theta}
	\end{subfigure}
	\caption{Comparison of $\dot{x}$ for $\SI{2000}{\hertz}$ and $\SI{2}{\hertz}$ and $\theta$ for $\SI{2000}{\hertz}$}
	\label{fig:cfh}
\end{figure}

\section{Conclusion}
In this assignment, we first defined the seesaw model in continuous time and analysed it (Section 2). In Section 3, we built an LQR controller, with the state variables available, so that it can follow a sequence of setpoints. This controller is able to make the steady-state error for $\theta$ and $x$ very low such that the system follows the setpoints very accurately. The closed-loop system is also stable and the reaction time is good enough.

\noindent
In practice we can only measure the output voltages so we designed a new controller in Section 4. After fine-tuning the controller again, we reduced the steady-state error significantly and there were no oscillations in the system. We also proved that the steady-state error however can never become zero. The closed-loop system is also stable and has a decent settling time.

\noindent
For the practical setup however, we managed to stabilize the sytem and make it have a good disturbance rejection, but the steady-state error is quite high. Also there seems to be an asymmetry between positive and negative setpoints for $\theta$. We also listed a few explanations for the deviations between simulations and experiments. Besides this, the practical system also seems to be quite resilient against relatively high noise levels and high cut-off frequencies.
\end{document}