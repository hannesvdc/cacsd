clear;
close all;
%% Load data
load data_disturbance.mat;
%% Plot data
plot_data(input_dist,reference_dist,xy_states_dist,xdotydot_dist)