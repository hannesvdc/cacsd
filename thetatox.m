function [ x ] = thetatox( theta )
%% THETATOX Convert a setpoint for theta to one in x
    Mc = 0.52;
    Dt = 0.125;
    Msw = 3.6;
    Dc = 0.058;
    x = (Mc*Dt+Msw*Dc)*tan(theta)/Mc;
end

