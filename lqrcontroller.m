% Create the model to perform LQR on
Clqr = eye(4);
Dlqr = zeros(4, 1);

lqrmodel = ss(A, B, Clqr, Dlqr);

% Create the LQR controller
Q = diag([1000 4000 300 300]);
R = 1;
N = zeros(4, 1);
[K, S, e] = lqr(lqrmodel, Q, R, N);
Nx = [eye(2) ; zeros(2)];

% Poles closed loop system
close all;
plot(eig(A-B*K),'o','Linewidth',2);
hold on;
plot(zeros(1e6,1),linspace(-3,3,1e6),'r-','Linewidth',2);
xlabel('Real axis','Interpreter','Latex');
ylabel('Imaginary axis','Interpreter','Latex');
axis([-65 2 -3 3]);
