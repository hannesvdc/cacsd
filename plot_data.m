function plot_data(input,reference,xy_states,xdotydot)
% PLOT_DATA plot the control actions, references and states
%   process_data(input,reference,xy_states,xdotydot) plots the control
%   actions, references and states given the inputs, reference and state
%   structs.
    time = input.time;
    time = time - time(1);
    input = input.signals.values;
    reference = reference.signals.values;
    x = xy_states.signals(1).values;
    theta = xy_states.signals(2).values;
    xd = xdotydot.signals(1).values;
    thetad = xdotydot.signals(2).values;
    %% Plot the results
    plot(time, reference,'Linewidth',2);
    hold on,
    plot(time, theta,'Linewidth',2);
    xlabel('time ($s$)','Interpreter','Latex','FontSize',16);
    ylabel('$\theta$ (degrees)','Interpreter','Latex','FontSize',16);
    legend('Reference','Output');
    figure;
    plot(time, input,'Linewidth',1.5);
    xlabel('time ($s$)','Interpreter','Latex','FontSize',16);
    ylabel('Input (volts)','Interpreter','Latex','FontSize',16);
    figure;
    plot(time,thetatox(reference*pi/180),'Linewidth',2);
    hold on;
    plot(time, x,'Linewidth',2);
    xlabel('time ($s$)','Interpreter','Latex','FontSize',16);
    ylabel('$x$','Interpreter','Latex','FontSize',16);
    legend('Reference','Output');
    figure;
    plot(time, xd,'Linewidth',2);
    xlabel('time ($s$)','Interpreter','Latex','FontSize',16);
    ylabel('$\dot{x}$','Interpreter','Latex','FontSize',16);
    figure;
    plot(time, thetad,'Linewidth',2);
    xlabel('time ($s$)','Interpreter','Latex','FontSize',16);
    ylabel('$\dot{\theta}$','Interpreter','Latex','FontSize',16);
end




