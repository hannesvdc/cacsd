%% linear stability analysis
poles  = pole(model);
tzeros = tzero(model);
pzmap(model);   % => not stable
legend('poles')

O   = obsv(model);
rO  = rank(O); % observable
Co  = ctrb(model);
rCo = rank(Co); % controllable => stabilizable and detectable and minimal
disp(['Rank of the controllability matrix: ', num2str(rO)]);
disp(['Rank of the observability matrix: ', num2str(rCo)]);



