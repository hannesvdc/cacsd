clear;
close all;
%% Load data
load data_freq.mat;
%% Plot data
plot_data(input_highfreq2000,reference_highfreq2000,xy_states_highfreq2000,xdotydot_highfreq2000);
plot_data(input_lowfreq,reference_lowfreq,xy_states_lowfreq,xdotydot_lowfreq);