%% Create the Statespace model in continuous time for the seesaw
% Physical constants
Rm = 2.6;
Kt = 0.00767;
Km = 0.00767;
Kg = 3.71;
etam = 1;
etag = 1;
Mc = 0.52;
Msw = 3.6;
Jsw = 0.3950;
Dt = 0.1250;
Dc = 0.058;
r = 0.00635;
g = 9.81;
Beq = 0.9;
Bsw = 0;

% System matrices
a31 = -Mc*Dt*g/Jsw;
a32 = (-g*Mc*Rm*r^2*Jsw + Mc*Dt*Rm*r^2*g*Msw*Dc)/(Rm*r^2*Jsw*Mc);
a33 = (-Jsw*etag*Kg^2*etam*Kt*Km - Jsw*Beq*Rm*r^2 - Mc*Dt^2*etag*Kg^2*etam*Kt*Km - Mc*Dt^2*Beq*Rm*r^2)/(Rm*r^2*Jsw*Mc);
a34 = -Dt*Bsw/Jsw;
a41 = -g*Mc/Jsw;
a42 = g*Msw*Dc/Jsw;
a43 = (-etag*Kg^2*etam*Kt*Km*Dt-Beq*Rm*r^2*Dt)/(Rm*r^2*Jsw);
a44 = -Bsw/Jsw;

b3 = (Jsw*etag*Kg*etam*Kt*r + Mc*Dt^2*etag*Kg*etam*Kt*r)/(Rm*r^2*Jsw*Mc);
b4 = (etag*Kg*etam*Kt*Dt)/(r*Rm*Jsw);

c1 = 4.41/0.456;
c2 = 3.166/(pi*15/180);

A = [[ 0 0 1 0]; [0 0 0 1]; [a31 a32 a33 a34]; [a41 a42 a43 a44]];
B = [0 0 b3 b4]';
C = [[c1 0 0 0]; [0 c2 0 0]];
D = zeros(2,1);

% and create the state space model.
model = ss(A, B, C, D);

% Quantization effects
WL = 20/2^16;
scalematrix = diag([1/c1, 1/c2]);
wc = 2*2*pi; %10
Ts = 0.05;

